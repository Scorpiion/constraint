// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.


library constraint.core_constraints.min_max.test;


import 'package:constrain/core_constraints.dart';
import 'package:constrain/constraint.dart';
import 'package:constrain/metadata.dart';
import 'package:unittest/unittest.dart';

main() {

  group('Min', () {
    group('produces correct description', () {
      test('when isInclusive', () {
        expect(const Min(12.3).description,
          equals("must be greater than or equal to 12.3"));
      });

      test('when isInclusive', () {
        expect(const Min(12.3, isInclusive: false).description,
          equals("must be greater than 12.3"));
      });
    });


    group('validate', () {
      group('returns empty set', () {
        group('when isInclusive is true', () {
          runTests(Comparable value, Comparable valueGreaterThan ) {
            final min = new Min(value);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(min, value);
            }

            test('and value equals min', () {
              expect(validate(value), isEmpty);
            });

            test('and value greater than min', () {
              expect(validate(valueGreaterThan), isEmpty);
            });
          }

          group('and type is num', () {
            runTests(12.2, 12.3);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now(),
              new DateTime.now().add(const Duration(seconds:3)));
          });
        });

        group('when isInclusive is false', () {
          runTests(Comparable value, Comparable valueGreaterThan ) {
            final min = new Min(value, isInclusive: false);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(min, value);
            }

            test('and value greater than min', () {
              expect(validate(valueGreaterThan), isEmpty);
            });
          }

          group('and type is num', () {
            runTests(12.2, 12.3);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now(),
              new DateTime.now().add(const Duration(seconds:3)));
          });
        });
      });

      group('returns set of violations', () {
        group('when isInclusive is true', () {
          runTests(Comparable value, Comparable valueLessThan ) {
            final min = new Min(value, isInclusive: true);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(min, value);
            }

            test('and value less than min', () {
              expect(validate(valueLessThan), hasLength(1));
            });
          }

          group('and type is num', () {
            runTests(12.2, 12.1);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now(),
              new DateTime.now().subtract(const Duration(seconds:3)));
          });
        });

        group('when isInclusive is false', () {
          runTests(Comparable value) {
            final min = new Min(value, isInclusive: false);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(min, value);
            }

            test('and value equals min', () {
              expect(validate(value), hasLength(1));
            });
          }

          group('and type is num', () {
            runTests(12.2);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now());
          });
        });
      });
    });

  });

  group('Max', () {
    group('produces correct description', () {
      test('when isInclusive', () {
        expect(const Max(12.3).description,
        equals("must be less than or equal to 12.3"));
      });

      test('when isInclusive', () {
        expect(const Max(12.3, isInclusive: false).description,
        equals("must be less than 12.3"));
      });
    });


    group('validate', () {
      group('returns empty set', () {
        group('when isInclusive is true', () {
          runTests(Comparable value, Comparable valueLessThan ) {
            final max = new Max(value);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(max, value);
            }

            test('and value equals max', () {
              expect(validate(value), isEmpty);
            });

            test('and value less than max', () {
              expect(validate(valueLessThan), isEmpty);
            });
          }

          group('and type is num', () {
            runTests(12.2, 12.1);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now(),
              new DateTime.now().subtract(const Duration(seconds:3)));
          });
        });

        group('when isInclusive is false', () {
          final max = const Max(12.2, isInclusive: false);

          Set<ConstraintViolation> validate(num value) {
            return _validate(max, value);
          }

          test('and value less than max', () {
            expect(validate(12.1), isEmpty);
          });
        });
      });

      group('returns set of violations', () {
        group('when isInclusive is true', () {
          final max = const Max(12.2);

          Set<ConstraintViolation> validate(num value) {
            return _validate(max, value);
          }

          test('and value greater than max', () {
            expect(validate(12.3), hasLength(1));
          });
        });

        group('when isInclusive is false', () {
          runTests(Comparable value, Comparable valueLessThan ) {
            final max = new Max(value, isInclusive: false);

            Set<ConstraintViolation> validate(Comparable value) {
              return _validate(max, value);
            }

            test('and value equals max', () {
              expect(validate(value), hasLength(1));
            });

            test('and value greater than max', () {
              expect(validate(valueLessThan), hasLength(1));
            });
          }

          group('and type is num', () {
            runTests(12.2, 12.3);
          });

          group('and type is DateTime', () {
            runTests(new DateTime.now(),
              new DateTime.now().add(const Duration(seconds:3)));
          });
        });
      });
    });

  });
}

class TestConstraintValidationContext extends ConstraintValidationContext {
  Constraint constraint;
  dynamic owner;
  Set<ConstraintViolation> violations = new Set();

  void addViolation({String reason, ViolationDetails details}) {
    violations.add(new ConstraintViolation(new ConstraintDescriptor(constraint),
        "null", "null", [], "null", "null"));
  }



}

Set<ConstraintViolation> _validate(Constraint constraint, value) {
  var context = new TestConstraintValidationContext()
    ..constraint = constraint;
  constraint.validate(value, context);
  return context.violations;
}
