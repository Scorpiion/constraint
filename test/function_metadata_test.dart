// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.metadata.function.test;

import 'package:constrain/constraint.dart';
import 'package:constrain/metadata.dart';
import 'package:unittest/unittest.dart';
import 'package:option/option.dart';
import 'domain_model.dart';

main() {
    TypeDescriptorResolver t = new TypeDescriptorResolver();

    group('resolveForFunction', () {
      test('when function is null should return None', () {
        expect(t.resolveForFunction(null), equals(const None()));
      });

      test('when function has no constraints should return None', () {
        expect(t.resolveForFunction(mrNoConstraints), equals(const None()));
      });

      test('when function is a core dart function should return None', () {
        expect(t.resolveForFunction(print), equals(const None()));
      });

      group('when function has a parameter constraint should return a FunctionDescriptor', () {
        descOpt() => t.resolveForFunction(singleConstraint);
        desc() => descOpt().get();

        test('that is some', () {
          expect(descOpt(), new isInstanceOf<Some>());
        });

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with non empty positional parameters descriptors', () {
          expect(desc().positionalParameterDescriptors, isNot(isEmpty));
        });

        test('with positional parameters descriptors length == function parameter length', () {
          expect(desc().positionalParameterDescriptors, hasLength(2));
        });

        test('with a single positional parameter descriptor that is Some', () {
          expect(desc().positionalParameterDescriptors.where((o) => o.nonEmpty()),
              hasLength(1));
        });

        test('with the first parameter descriptor that is Some', () {
          expect(desc().positionalParameterDescriptors[0], new isInstanceOf<Some>());
        });

        test('with empty named parameters descriptors', () {
          expect(desc().namedParameterDescriptors, isEmpty);
        });

        test('with one constraint on first parameter', () {
          expect(desc().positionalParameterDescriptors[0].get().constraintDescriptors,
              hasLength(1));
        });

        test('with expected constraint on parameter', () {
          expect(desc().positionalParameterDescriptors[0].get()
              .constraintDescriptors.first.constraint,
              new isInstanceOf<NotNull>());
        });

        test('with no type descriptor on parameter', () {
          expect(desc().positionalParameterDescriptors[0].get()
              .typeDescriptor,
              equals(const None()));
        });

        test('with caching of descriptors', () {
          expect(desc(), same(desc()));
        });

      });

      group('when parameter type has a constraint should return a FunctionDescriptor', () {
        descOpt() => t.resolveForFunction(constraintOnParamType);
        desc() => descOpt().get();
        typeDescriptorOpt() => desc().positionalParameterDescriptors[0].get().typeDescriptor;
        typeDescriptor() => typeDescriptorOpt().get();

        test('that is some', () {
          expect(descOpt(), new isInstanceOf<Some>());
        });

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with the first parameter descriptor that is Some', () {
          expect(desc().positionalParameterDescriptors[0], new isInstanceOf<Some>());
        });

        test('with no constraints on first parameter', () {
          expect(desc().positionalParameterDescriptors[0].get()
              .constraintDescriptors, isEmpty);
        });

        test('with a type descriptor on parameter', () {
          expect(typeDescriptorOpt(), new isInstanceOf<Some>());
        });

        test('with a type descriptor of expected type on parameter', () {
          expect(typeDescriptor().type, equals(TestModel3));
        });

        test('with a type descriptor with expected constraint on parameter - #title', () {
          final titleDesc = typeDescriptor().memberDescriptorsMap[#title];
          expect(titleDesc, isNotNull);
          expect(titleDesc.constraintDescriptors, hasLength(1));
          expect(titleDesc.constraintDescriptors.first.constraint,
              equals(const Ensure(endsWithDot)));
        });

        test('with a type descriptor with expected constraint on parameter - #testModel1', () {
          final testModel1Desc = typeDescriptor().memberDescriptorsMap[#testModel1];
          expect(testModel1Desc, isNotNull);
          expect(testModel1Desc.constraintDescriptors, isEmpty);
          expect(testModel1Desc.typeDescriptor, new isInstanceOf<Some>());
//          expect(testModel1Desc.typeDescriptor, isNot(isEmpty));
        });

      });

      group('when function has a named parameter constraint should return a FunctionDescriptor', () {
        descOpt() => t.resolveForFunction(singleNamedConstraint);
        desc() => descOpt().get();
        blahDescOpt() => desc().namedMemberDescriptor(#blah);
        blahDesc() => blahDescOpt().get();

        test('that is some', () {
          expect(descOpt(), new isInstanceOf<Some>());
        });

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with an empty positional parameters descriptors', () {
          expect(desc().positionalParameterDescriptors, isNot(isEmpty));
        });

        test('with positional parameters descriptors length == function parameter length', () {
          expect(desc().positionalParameterDescriptors, hasLength(1));
        });

        test('with a no positional parameter descriptor that is Some', () {
          expect(desc().positionalParameterDescriptors.where((o) => o.nonEmpty()),
              hasLength(0));
        });

//        test('with the first parameter descriptor that is Some', () {
//          expect(desc().positionalParameterDescriptors[0], new isInstanceOf<Some>());
//        });

        test('with non empty named parameters descriptors', () {
          expect(desc().namedParameterDescriptors, isNot(isEmpty));
        });

        test('with a descriptor for named parameter', () {
          expect(blahDescOpt(), new isInstanceOf<Some>());
        });

        test('with a single constraint on named parameter', () {
          expect(blahDesc().constraintDescriptors, hasLength(1));
        });

        test('with expected constraint on parameter', () {
          expect(blahDesc()
              .constraintDescriptors.first.constraint,
              new isInstanceOf<NotNull>());
        });

        test('with no type descriptor on parameter', () {
          expect(blahDesc().typeDescriptor, equals(const None()));
        });

        test('with caching of descriptors', () {
          expect(desc(), same(desc()));
        });

      });

      group('when function has a return constraint should return a FunctionDescriptor', () {
        descOpt() => t.resolveForFunction(constraintOnReturn);
        desc() => descOpt().get();
        returnDesc() => desc().returnDescriptor.get();

        test('that is some', () {
          expect(descOpt(), new isInstanceOf<Some>());
        });

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with empty positional parameters descriptors', () {
          expect(desc().positionalParameterDescriptors, isEmpty);
        });

        test('with empty named parameters descriptors', () {
          expect(desc().namedParameterDescriptors, isEmpty);
        });

        test('with non empty return descriptor', () {
          expect(desc().returnDescriptor, new isInstanceOf<Some>());
        });

        test('with one constraint on return', () {
          expect(returnDesc().constraintDescriptors,
              hasLength(1));
        });

        test('with expected constraint on return', () {
          expect(returnDesc().constraintDescriptors.
              first.constraint,
              new isInstanceOf<NotNull>());
        });

        test('with no type descriptor on return', () {
          expect(returnDesc().typeDescriptor, equals(const None()));
        });

        test('with caching of descriptors', () {
          expect(desc(), same(desc()));
        });

      });

      group('when return type has a constraint should return a FunctionDescriptor', () {
        descOpt() => t.resolveForFunction(constraintOnReturnType);
        desc() => descOpt().get();
        returnDescriptorOpt() => desc().returnDescriptor;
        returnDescriptor() => returnDescriptorOpt().get();
        typeDescriptorOpt() => returnDescriptor().typeDescriptor;
        typeDescriptor() => typeDescriptorOpt().get();

        test('that is some', () {
          expect(descOpt(), new isInstanceOf<Some>());
        });

        test('that is not null', () {
          expect(desc(), isNotNull);
        });

        test('with the first parameter descriptor that is Some', () {
          expect(returnDescriptorOpt(), new isInstanceOf<Some>());
        });

        test('with no constraints on first parameter', () {
          expect(returnDescriptor().constraintDescriptors, isEmpty);
        });

        test('with a type descriptor on parameter', () {
          expect(typeDescriptorOpt(), new isInstanceOf<Some>());
        });

        test('with a type descriptor of expected type on parameter', () {
          expect(typeDescriptor().type, equals(TestModel3));
        });

        test('with a type descriptor with expected constraint on parameter - #title', () {
          final titleDesc = typeDescriptor().memberDescriptorsMap[#title];
          expect(titleDesc, isNotNull);
          expect(titleDesc.constraintDescriptors, hasLength(1));
          expect(titleDesc.constraintDescriptors.first.constraint,
              equals(const Ensure(endsWithDot)));
        });

        test('with a type descriptor with expected constraint on parameter - #testModel1', () {
          final testModel1Desc = typeDescriptor().memberDescriptorsMap[#testModel1];
          expect(testModel1Desc, isNotNull);
          expect(testModel1Desc.constraintDescriptors, isEmpty);
          expect(testModel1Desc.typeDescriptor, new isInstanceOf<Some>());
//          expect(testModel1Desc.typeDescriptor, isNot(isEmpty));
        });

      });


//
//      group('should include inherited member constraints', () {
//        group('when no constraints on current class', () {
//          descOpt() => t.resolveFor(ChildModel);
//          desc() => descOpt().get();
//          mm() => desc().memberDescriptorsMap;
//          nameConstraints() => mm()[#name].constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and provide access in members map', () {
//            expect(mm(), hasLength(1));
//            expect(mm()[#name], isNotNull);
//            expect(nameConstraints(), hasLength(1));
//          });
//
//        });
//
//        group('when subclass has additional constraints on property', () {
//          descOpt() => t.resolveFor(ChildModel2);
//          desc() => descOpt().get();
//          mm() => desc().memberDescriptorsMap;
//          nameConstraints() => mm()[#name].constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and provide access in members map', () {
//            expect(mm(), hasLength(1));
//            expect(mm()[#name], isNotNull);
//            expect(nameConstraints(), hasLength(2));
//          });
//
//        });
//
//      });
//      group('should include inherited type constraints', () {
//        group('when no constraints on current class', () {
//          descOpt() => t.resolveFor(ChildModel3);
//          desc() => descOpt().get();
//          cds() => desc().constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and include in current type decriptor', () {
//            expect(cds(), hasLength(1));
//            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
//          });
//
//        });
//
//        group('when subclass has additional constraints on class', () {
//          descOpt() => t.resolveFor(ChildModel4);
//          desc() => descOpt().get();
//          cds() => desc().constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and include in current type decriptor', () {
//            expect(cds(), hasLength(2));
//            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
//            expect(cds().last.constraint.validator, equals(blahInRange));
//          });
//
//        });
//      });
//
//      group('should include constraints inherited from interfaces', () {
//        group('and include in member descriptors', () {
//          descOpt() => t.resolveFor(ChildModel5);
//          desc() => descOpt().get();
//          mm() => desc().memberDescriptorsMap;
//          nameCds() => mm()[#name].constraintDescriptors;
//          nameMatchers() => nameCds().map((cd) => cd.constraint.validator);
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and provide access in members map', () {
//            expect(mm(), hasLength(1));
//            expect(mm()[#name], isNotNull);
//            expect(nameCds(), hasLength(2));
//            expect(nameMatchers(), contains(endsWithDot));
//            expect(nameMatchers(), contains(isNotNull));
//          });
//
//        });
//
//        group('and include type constraints', () {
//          descOpt() => t.resolveFor(ChildModel6);
//          desc() => descOpt().get();
//          cds() => desc().constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and include in current type decriptor', () {
//            expect(cds(), hasLength(2));
//            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
//            expect(cds().last.constraint.validator, equals(blahInRange));
//          });
//
//        });
//      });
//
//      group('should include constraints inherited from mixins', () {
//        descOpt() => t.resolveFor(ChildModel7);
//        desc() => descOpt().get();
//        mm() => desc().memberDescriptorsMap;
//        blahCds() => mm()[#blah].constraintDescriptors;
//        blahMatchers() => blahCds().map((cd) => cd.constraint.validator);
//
//        group('and include in member descriptors', () {
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and provide access in members map', () {
//            expect(mm(), hasLength(1));
//            expect(mm()[#blah], isNotNull);
//            expect(blahCds(), hasLength(1));
//            expect(blahMatchers(), contains(isNotNull));
//          });
//
//        });
//
//        group('and include type constraints', () {
//          cds() => desc().constraintDescriptors;
//
//          test('', () {
//            expect(descOpt(), new isInstanceOf<Some>());
//          });
//
//          test('and include in current type decriptor', () {
//            expect(cds(), hasLength(1));
//            expect(cds().first.constraint.validator, equals(blahIsNullOrPositive));
//          });
//
//        });
//
//      });
//
//      group('should detect collections', () {
//        descOpt() => t.resolveFor(TestModel4);
//        desc() => descOpt().get();
//        mm() => desc().memberDescriptorsMap;
//        modelsMemDesc() => mm()[#models];
//        modelsTypeDescOpt() => modelsMemDesc().typeDescriptor;
//        modelsTypeDesc() => modelsTypeDescOpt().get();
//        modelsCds() => modelsMemDesc().constraintDescriptors;
//        blahMatchers() => modelsCds().map((cd) => cd.constraint.validator);
//
//        test('', () {
//          expect(descOpt(), new isInstanceOf<Some>());
//          expect(mm(), hasLength(1));
//          expect(modelsMemDesc(), isNotNull);
//        });
//
//        test('and set isMultivalued', () {
//          expect(modelsMemDesc().isMultivalued, equals(true));
//        });
//
//        test('and set isGeneric', () {
//          expect(modelsMemDesc().isGeneric, equals(true));
//        });
//
//        test('and have type that is the generic type', () {
//          expect(modelsTypeDescOpt(), new isInstanceOf<Some>());
//          expect(modelsTypeDesc().type, equals(TestModel1));
//        });
//

    });

    /*
     * Must cover:
     * * caching - done
     * * constraints on types - done
     * * named parameters - done
     * * returns - done
     * * constraints on return types - done
     *
     * * Methods
     *   * inherited constraints
     *     * on params
     *     * on returns
     *     * from superclasses
     *     * from superinterfaces
     *   * static methods???
     *
     * * sanity test on collections
     */

//    new Timer(const Duration(minutes: 10), () {});
}


String mrNoConstraints(int blah) => '$blah';

String singleConstraint(@NotNull() int blah, String foo) => '$blah';

String constraintOnParamType(TestModel3 model) => '';

String singleNamedConstraint(String foo, { @NotNull() int blah } ) => '$blah';

@NotNull() String constraintOnReturn() => '';

TestModel3 constraintOnReturnType() => null;

