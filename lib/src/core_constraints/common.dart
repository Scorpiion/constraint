// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.common;

import 'package:constrain/constraint.dart';
import '../preconditions.dart';

abstract class CoreConstraint<T> implements Constraint<T> {
  final ConstraintGroup group;

  const CoreConstraint({ ConstraintGroup group }) :
    this.group = group != null ? group : const DefaultGroup();

  @override
  void validate(T value, ConstraintValidationContext context) {
    if (value != null) {
      _doValidate(value, context);
    }
  }

  void _doValidate(T value, ConstraintValidationContext context) {
    if (!isValid(value)) {
      context.addViolation();
    }
  }

  bool isValid(T value);

  Map toJson() => {
    'type': runtimeType.toString(),
    'description': description,
    'group': group.runtimeType.toString()
  };

}


//class CompositeConstraint<T> implements Constraint<T> {
//  final ConstraintGroup group;
//  final List<CoreConstraint> constraints;
//
//  const CompositeConstraint(this.constraints, { this.group });
//
//  void validate(T value, ConstraintValidationContext context) {
//    constraints.forEach((c) => c._doValidate(value, context));
//  }
//
//  String get description => constraints.map((c) => c.description)
//    .join(' and ');
//
//  Map toJson() => {
//      'type': runtimeType.toString(),
//      'description': description,
//      'group': group.runtimeType.toString()
//  };
//
//}