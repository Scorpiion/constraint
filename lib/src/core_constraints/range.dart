// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.core_constraints.range;

import 'common.dart';

/**
 * The annotated element must be a [Comparable] whose value is within the range
 * of the specified [min] and [max]. If [isMinInclusive] is true then the value
 * may also be equal to [min], if [isMaxInclusive] is true then the value
 * may also be equal to [max].
 *
 * Supported types are anything [Comparable] such as:
 *
 * * [num]
 * * [DateTime]
 *
 * _Note: doubles incur rounding errors and are therefore not reliably handled._
 *
 *  null elements are considered valid.
 */
class Range extends CoreConstraint<Comparable> {
  final Comparable min;
  final Comparable max;

  /// if true then the value may also be equal to the [max].
  final bool isMinInclusive;
  final bool isMaxInclusive;

  const Range(this.min, this.max,
      { bool this.isMinInclusive: true, bool this.isMaxInclusive: true });

  @override
  String get description =>
      "must be less than ${isMaxInclusive ? 'or equal to ' : ''}$max " +
      "and be greater than ${isMinInclusive ? 'or equal to ' : ''}$min";

  @override
  bool isValid(Comparable value) {
    final lessThen = value.compareTo(min);
    final greaterThen = value.compareTo(max);

    return (isMinInclusive ? lessThen >= 0 : lessThen > 0) &&
           (isMaxInclusive ? greaterThen <= 0 : greaterThen < 0);
  }
}

