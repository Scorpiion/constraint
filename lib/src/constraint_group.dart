// Copyright (c) 2014, The Constrain project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library constraint.group;

/// A group of constraints. [ConstraintGroup]s are used to restrict which
/// constraints are validated during a validation.
///
/// Matching for ConstraintGroups is done via the [implies] method. This
/// method should return true to indicate that the constraint should be
/// validated
abstract class ConstraintGroup {
  bool implies(ConstraintGroup other);
}

/// A simple [ConstraintGroup] that matches itself and the [DefaultGroup]
abstract class SimpleConstraintGroup implements ConstraintGroup {
  const SimpleConstraintGroup();

  bool implies(ConstraintGroup other) =>
      this == other || other == const DefaultGroup();
}

/// The default [ConstraintGroup]. The default group is always validated
class DefaultGroup extends SimpleConstraintGroup {
  const DefaultGroup();
}

/// A [ConstraintGroup] composed of other [ConstraintGroup]s (a la [GOF
/// Composite Pattern](http://en.wikipedia.org/wiki/Composite_pattern))
class CompositeGroup implements ConstraintGroup {
  final Iterable<ConstraintGroup> _children;
  const CompositeGroup(this._children);

  @override
  bool implies(ConstraintGroup other) =>
    _children.any((child) => child.implies(other));
}

